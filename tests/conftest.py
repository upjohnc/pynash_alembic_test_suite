import pytest

from alembic.command import downgrade, upgrade
from alembic.config import Config

ALEMBIC_CONFIG = '/usr/app/alembic.ini'


@pytest.fixture(autouse=True, scope='session')
def apply_migrations():
    config = Config(ALEMBIC_CONFIG)
    upgrade(config, 'head')
    yield
    downgrade(config, 'base')
