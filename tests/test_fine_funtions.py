import datetime as dt
from decimal import Decimal

import psycopg2
import pytest
from psycopg2.extras import DictCursor

import fine_module as fm

USER = 'postgres'


def postgres_connection():
    return psycopg2.connect(host='db', port=5432, dbname='test_suite_db', user=USER, )


@pytest.fixture
def clean_db():
    yield
    with postgres_connection() as connection:
        cursor_ = connection.cursor(cursor_factory=DictCursor)
        cursor_.execute(f"""SELECT tablename FROM pg_tables
                            WHERE tableowner = '{USER}' AND schemaname = 'public'
                                  AND tablename != 'alembic_version'""")
        all_tables = ', '.join([i['tablename'] for i in cursor_.fetchall()])
        cursor_.execute(f"""TRUNCATE TABLE {all_tables} CASCADE;""")


@pytest.fixture
def insert_test_data():
    with postgres_connection() as connection:
        connection.cursor().execute("INSERT INTO unit_test_table (num, data) VALUES (%s, %s)", (100, "abcdef"))


def mock_snf(*args, **kwargs):
    return postgres_connection()


def test_get_all_records(monkeypatch, insert_test_data, clean_db):
    monkeypatch.setattr(fm.snf, 'connect', mock_snf)
    result = fm.get_all_records()
    assert (1, 100, 'abcdef') == result


def test_insert_accounting_one_record(monkeypatch, clean_db):
    date_sale = dt.datetime(2019, 1, 1)
    value = Decimal(12.3)
    insert_values = dict(date_sale=date_sale, value=value)
    monkeypatch.setattr(fm.snf, 'connect', mock_snf)
    fm.insert_accounting_records(insert_values)
    with postgres_connection() as connection:
        cursor_ = connection.cursor(cursor_factory=DictCursor)
        cursor_.execute("""Select date_sale, value from accounting;""")
        result = cursor_.fetchall()

    assert len(result) == 1
    assert result[0]['date_sale'] == date_sale
    assert result[0]['value'] == value


def test_insert_accounting_two_record(monkeypatch, clean_db):
    value_one = 12.3
    value_two = 15.4
    insert_values = [dict(date_sale=dt.datetime(2019, 1, 1), value=value_one),
                     dict(date_sale=dt.datetime(2018, 12, 15), value=value_two),
                     ]
    monkeypatch.setattr(fm.snf, 'connect', mock_snf)
    fm.insert_accounting_records(insert_values)
    with postgres_connection() as connection:
        cursor_count = connection.cursor(cursor_factory=DictCursor)
        cursor_count.execute("""SELECT count(1) as count FROM accounting;""")
        results_count = cursor_count.fetchone()
        cursor_sum = connection.cursor(cursor_factory=DictCursor)
        cursor_sum.execute("""SELECT SUM(value) as value_sum FROM accounting;""")
        result_sum = cursor_sum.fetchone()

    assert results_count['count'] == 2
    assert round(result_sum['value_sum'], 10) == round(sum([Decimal(i) for i in [value_one, value_two]]), 10)
