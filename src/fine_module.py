import snowflake.connector as snf

snf_parameters = dict(user='me',
                      db='fake_db',
                      warehouse='fake_wh',
                      schema='fake_schema',
                      )


def get_all_records():
    with snf.connect(**snf_parameters) as connection:
        cursor_ = connection.cursor()
        cursor_.execute("""SELECT * FROM unit_test_table;""")
        result = cursor_.fetchone()
        connection.commit()
        cursor_.close()
    return result


def insert_accounting_records(new_records):
    """
    Insert records into the accounting table
    :param new_records: list of dict or dict : dict with keys date_sale (datetime) and value (decimal)
    """
    if isinstance(new_records, dict):
        new_records = [new_records]
    columns = new_records[0].keys()

    def set_tuple_row(row):
        return tuple([row[column] for column in columns])

    values_as_list = list()
    for record in new_records:
        values_as_list.append(set_tuple_row(record))

    column_query_string = ','.join(['%s' for _ in range(len(columns))])
    insert_statement = f"INSERT INTO accounting ({','.join(columns)}) VALUES({column_query_string})"
    with snf.connect(**snf_parameters) as connection:
        cursor_ = connection.cursor()
        cursor_.executemany(insert_statement, values_as_list)
