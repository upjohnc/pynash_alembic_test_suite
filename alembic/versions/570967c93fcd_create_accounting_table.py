"""create accounting table

Revision ID: 570967c93fcd
Revises: e60b91abc4ef
Create Date: 2019-07-14 06:36:58.744916

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = '570967c93fcd'
down_revision = 'e60b91abc4ef'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('accounting',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('date_sale', sa.DateTime),
                    sa.Column('value', sa.DECIMAL),
                    )


def downgrade():
    op.drop_table('accounting')
