"""create unit_test_table

Revision ID: e60b91abc4ef
Revises:
Create Date: 2019-07-10 10:47:44.685986

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'e60b91abc4ef'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('unit_test_table',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('num', sa.Integer),
                    sa.Column('data', sa.String)
                    )


def downgrade():
    op.drop_table('unit_test_table')
