# Alembic for Snowflake Test Suite
This is a side project to see if I can use alembic and postgres in tests when the functions call Snowflake

For the set up of docker running locally, I followed:
https://www.saltycrane.com/blog/2019/01/how-run-postgresql-docker-mac-local-development/


## Basic Usage
- `docker-compose run --rm python bash`
- in separate terminal but under the same directory
    - only need to do one time
    - `docker-compose exec db psql -U postgres`
    - `create database test_suite_db;`
- in the python terminal
    - `make pytests`
- when done
    - exit the container
    - `docker-compose down` (will remove the postgres database)